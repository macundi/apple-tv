//
//  ServiceProvider.h
//  TopShelfNews
//
//  Created by Markus Stöbe on 27.10.15.
//  Copyright © 2015 Markus Stöbe. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <TVServices/TVServices.h>
#import "RssParser.h"

@interface ServiceProvider : NSObject <TVTopShelfProvider, RssDelegate>


@end

