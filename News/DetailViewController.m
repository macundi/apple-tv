//
//  DetailViewController.m
//  News
//
//  Created by Markus Stöbe on 11.10.15.
//  Copyright © 2015 Markus Stöbe. All rights reserved.
//

#import "DetailViewController.h"

@implementation DetailViewController

- (void)setDetailItem:(id)newDetailItem {
    if (_detailItem != newDetailItem) {
        _detailItem = newDetailItem;
        
        // Update the view.
        [self configureView];
    }
}

- (void)configureView {
    // Update the user interface for the detail item.
    if (self.detailItem) {

        //convert HTML-Text to clean RTF for displaying on screen        
        NSString *cleanText = [[[NSAttributedString alloc] initWithData:[self.detailItem.summary dataUsingEncoding:NSUTF8StringEncoding]
                                                                options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType,//NSRTFTextDocumentType,
                                                                          NSCharacterEncodingDocumentAttribute:@(NSUTF8StringEncoding)}
                                                     documentAttributes:nil error:nil] string];
        
        //Set the text
        self.detailText.text = cleanText;
        
        //load and set the image
        NSString *imageURL   = self.detailItem.imageURL;
        NSURL    *url        = [NSURL URLWithString:imageURL];
        NSData   *imageData  = [NSData dataWithContentsOfURL:url];
        //TODO:Move this to a background-thread...or else...
        UIImage   *image     = [UIImage imageWithData:imageData];
        self.detailImage.image = image;
        
        //adjust height of imagebox
        float factor = self.detailImageWidth.constant / image.size.width;
        self.detailImageWidth.constant  = image.size.width  * factor;
        self.detailImageHeight.constant = image.size.height * factor;
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [self configureView];
}


@end
