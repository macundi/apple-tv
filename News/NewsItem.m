//
//  NewsItem.m
//  News
//
//  Created by Markus Stöbe on 28.10.15.
//  Copyright © 2015 Markus Stöbe. All rights reserved.
//

#import "NewsItem.h"

@implementation NewsItem

-(instancetype)init {
    self = [super init];
    if (self) {
        self.title    = [[NSMutableString alloc] init];
        self.link     = [[NSMutableString alloc] init];
        self.summary  = [[NSMutableString alloc] init];
        self.imageURL = [[NSMutableString alloc] init];
        self.date     = [[NSMutableString alloc] init];
    }
    return self;
}

@end
