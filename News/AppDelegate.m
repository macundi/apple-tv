//
//  AppDelegate.m
//  News
//
//  Created by Markus Stöbe on 11.10.15.
//  Copyright © 2015 Markus Stöbe. All rights reserved.
//

#import "AppDelegate.h"
#import "ViewController.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    return YES;
}


- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<NSString *,id> *)options {
    
    NSURLComponents *tempURL = [NSURLComponents componentsWithString:url.absoluteString];
    tempURL.scheme = @"http";
  
    ViewController *tableViewController = (ViewController *)[self.window rootViewController];
    [tableViewController performSegueWithIdentifier:@"showPreselectedStory" sender:tempURL.string];

    return YES;
}

@end
