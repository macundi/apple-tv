//
//  RssParser.h
//  News
//
//  Created by Markus Stöbe on 11.10.15.
//  Copyright © 2015 Markus Stöbe. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NewsItem.h"

@class RssParser;

@protocol RssDelegate <NSObject>

- (void)RssParser:(RssParser*)parser didFinishParsingAndFoundNews:(NSArray*)foundStories;

@end

@interface RssParser : NSObject<NSXMLParserDelegate>

@property (nonatomic, weak)   id<RssDelegate> delegate; //someone who will print the news we have to tell
@property (nonatomic, strong) NSURL           *url;     //the url to parse, after setting the url you have to call loadNews

- (instancetype)initWithDelegate:(id<RssDelegate>)delegate;
- (void)loadNews;

@end
