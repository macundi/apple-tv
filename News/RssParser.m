//
//  RssParser.m
//  News
//
//  Created by Markus Stöbe on 11.10.15.
//  Copyright © 2015 Markus Stöbe. All rights reserved.
//

#import "RssParser.h"

@interface RssParser ()

@property (nonatomic, strong) NSXMLParser           *xmlParser;         //holds the parser-instance to go through the news
@property (nonatomic, strong) NSString              *currentElement;    //pointer to the element currently under investigation
@property (nonatomic, strong) NSMutableArray        *stories;           //array holding all parsed stories we found on Mac & i
@property (nonatomic, strong) NewsItem              *newsItem;          //each entry in the stories-array is of type newsItem

@end

@implementation RssParser

//******************************************************************************************************************
#pragma mark - lifecycle and class-methods
//******************************************************************************************************************
- (instancetype)initWithDelegate:(id<RssDelegate>)delegate {
    self = [self init];
    if (self) {
        self.delegate = delegate;
    }
    return self;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        self.stories = [[NSMutableArray alloc] init];
        self.url     = [NSURL URLWithString:@"http://www.heise.de/mac-and-i/news-atom.xml"]; //default-URL
    }
    return self;
}

- (void)loadNews {
    BOOL success;
    
    //clean up old news
    self.stories = [[NSMutableArray alloc] init];
    
    //if we got an url, try loading and parsing it
    if (self.url) {
        self.xmlParser  = [[NSXMLParser alloc] initWithContentsOfURL:self.url];
        
        if (self.xmlParser != nil) {
            [self.xmlParser setDelegate:self];
            [self.xmlParser setShouldResolveExternalEntities:NO];
            success = [self.xmlParser parse];
        }
        if (!success) {
            NSLog(@"something went wrong -- ERROR: %@", [self.xmlParser.parserError localizedDescription]);
        }
    }
}

//******************************************************************************************************************
#pragma mark - NSXMLParserDelegate
//******************************************************************************************************************
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)currentElementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary<NSString *,NSString *> *)attributeDict {

    self.currentElement = currentElementName;
    
    if ([currentElementName isEqualToString:@"entry"]) {
        //make a new empty newsItem if we find another entry
        self.newsItem = [[NewsItem alloc] init];
    }
    
}
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)currentElementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    //add newsItem to array if collection is finished
    if ([currentElementName isEqualToString:@"entry"]) {
        [self.stories addObject:self.newsItem];
    }
}
- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {

    //sort text-parts and append where appropriate
    if ([self.currentElement isEqualToString:@"title"]) {
        self.newsItem.title = [self.newsItem.title stringByAppendingString:string];
    }
    
    else if ([self.currentElement isEqualToString:@"id"]) {
        //kill unwanted chararcters from url first
        string = [string stringByReplacingOccurrencesOfString:@"\n" withString:@""];
        string = [string stringByReplacingOccurrencesOfString:@" " withString:@""];
        self.newsItem.link = [self.newsItem.link stringByAppendingString:string];
    }
    
    else if ([self.currentElement isEqualToString:@"summary"]) {
        self.newsItem.summary = [self.newsItem.summary stringByAppendingString:string];
    }
    
    else if ([self.currentElement isEqualToString:@"content"]) {
        NSArray *images = [self extractImageURLsFromDatastring:string];
        if (images.count > 0) {
            self.newsItem.imageURL = [images.firstObject copy];
        }
    }
    
    else if ([self.currentElement isEqualToString:@"published"]) {
        self.newsItem.date  = [self.newsItem.date stringByAppendingString:string];
    }
}
- (void)parserDidEndDocument:(NSXMLParser *)parser {
    //let our delegate know that we finished loading
    if (self.delegate != nil) {
        [self.delegate RssParser:self didFinishParsingAndFoundNews:[self.stories copy]];
    }
    
}

//******************************************************************************************************************
#pragma mark - HTML-Parsing for Images, Texts etc.
//******************************************************************************************************************
- (NSArray*)extractImageURLsFromDatastring:(NSString*)data {
    
    NSMutableArray *imageURLs = [[NSMutableArray alloc] init];
    //<img src=\".*\"
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"src=\"(.*?)\"" options:NSRegularExpressionCaseInsensitive error:nil];
    [regex enumerateMatchesInString:data options:0 range:NSMakeRange(0, [data length])
                         usingBlock:^(NSTextCheckingResult *result, NSMatchingFlags flags, BOOL *stop)
     {
         //add all URLs to array
         for (int i=0; i<[result numberOfRanges]; i++)
         {
             //extracts data-src-line
             NSString *url = [data substringWithRange:[result rangeAtIndex:i]];
             //clean URL
             url = [url stringByReplacingOccurrencesOfString:@"src=\"" withString:@""];
             url = [url stringByReplacingOccurrencesOfString:@"\"" withString:@""];
             //add to array
             [imageURLs addObject:url];
         }
     }];
    
    return [NSArray arrayWithArray:imageURLs];
}





@end
